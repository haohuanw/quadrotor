#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if(argc < 3){
        printf("Didn't provide enough criteria.\n");
        printf("./config_gen <filename> <outputFile>\n");
    }
    const char* readFilename = argv[1];
    const char* outFilename = argv[2]; 
    float init_position[4];
    memset(init_position, 0, 6*sizeof(float));
    float time,x,y,z,roll,pitch,yaw;
    int counter = 0;
    FILE *readfp = fopen(readFilename, "r");
    while(fscanf(readfp, "%f,%f,%f,%f,%f,%f,%f\n", &time, &x, &y, &z, &roll, &pitch, &yaw) != EOF){
        ++counter;
        init_position[0] += x;
        init_position[1] += y;
        init_position[2] += z;
        init_position[3] += yaw;
    }
    fclose(readfp);
    int i;
    for (i = 0; i < 4; ++i) {
        init_position[i]/=counter;
    }
    FILE *writefp = fopen(outFilename, "w");
    fprintf(writefp, "%f %f %f %f\n", init_position[0], init_position[1], init_position[2], init_position[3]);
    for (i=1; i<2; ++i){
	fprintf(writefp, "%f %f %f %f\n", init_position[0], init_position[1], init_position[2]-1*i, init_position[3]);
    }
    fclose(writefp);
    return 0;
}
