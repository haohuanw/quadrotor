//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"
#include <math.h>
// Define outer loop controller 
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up 1575 // Upper saturation PWM limit.
#define thrust_PWM_base 1516 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1425 // Lower saturation PWM limit. 

// ROLL
#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1498  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1380 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1490 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit. 

// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1503 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right). 

float PID(float error, float vel, float kp, float ki, float kd);

// Outer loop controller to generate PWM signals for the Naza-M autopilot
void auto_control(float *pose, float *set_points, int16_t* channels_ptr,int drop_off,int motor_off)
{
	// pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
	// set_points (size 8):  reference state (you need to set this!)
	//                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot}

	// channels_ptr (8-element array of PWM commands to generate in this function)
	// Channels for you to set:
	// [0] = thrust
	// [1] = roll
	// [2] = pitch
	// [3] = yaw
	float thrustkp = 100;
	float thrustki = 0;
	float thrustkd = 20;
	float rollkp = 75;
	float rollki = 0;
	float rollkd = 10;
	float pitchkp = 55;
	float pitchki = 0;
	float pitchkd = 50;
	float yawkp = 30;
	float yawki = 0;
	float yawkd = 20;

	//printf("auto_control accessed");
	if (drop_off==0){
        if(motor_off){
        channels_ptr[0] = 1082; //thrust_PWM_base;
		channels_ptr[1] = 1919; //roll_PWM_base; 
		channels_ptr[2] = 1073; //pitch_PWM_base;
		channels_ptr[3] = 1112; //yaw_PWM_base;

        }
        else{
		// Tested and correct
        //printf("pose: %lf setpoint: %f roll error: %f \n", pose[1], set_points[1], pose[1]-set_points[1]);
		channels_ptr[0] = thrust_PWM_base + (int)(PID(pose[2] - set_points[2], pose[6],thrustkp, thrustki, thrustkd));
		//channels_ptr[1] = roll_PWM_base +(int)(PID(pose[1] - set_points[1],pose[5],rollkp, rollki, rollkd));
		channels_ptr[2] = pitch_PWM_base -(int)(PID(pose[0] - set_points[0], pose[4],pitchkp, pitchki, pitchkd));
		//channels_ptr[3] = yaw_PWM_base + (int)(PID(pose[3] - set_points[3], pose[7],yawkp, yawki, yawkd));
	
        channels_ptr[0] = fmax(fmin(channels_ptr[0],thrust_PWM_up),thrust_PWM_down);
        //channels_ptr[1] = fmax(fmin(channels_ptr[1],roll_PWM_left),roll_PWM_right);
        channels_ptr[2] = fmax(fmin(channels_ptr[2],pitch_PWM_forward),pitch_PWM_backward);
        //channels_ptr[3] = fmax(fmin(channels_ptr[3],yaw_PWM_ccw),yaw_PWM_cw);
        }
        
    }
    else{
        
		channels_ptr[1] = thrust_PWM_base;
		channels_ptr[2] = roll_PWM_base; 
		channels_ptr[0] = pitch_PWM_base;
		channels_ptr[3] = yaw_PWM_base;
	}
	//printf("PID data: thrust %d roll %d pitch %d yaw %d\n", channels_ptr[0],channels_ptr[1],channels_ptr[2],channels_ptr[3]) ;
	return;
}

//Modify further to play with PI and PID controller as well as gains
float PID(float err, float vel, float kp, float ki, float kd)
{
	//printf("error: %f\n", err);
	//static int flag = 0;
	//static float prev_err =  -1.0;
	// Simple P controller
	//printf("PID accessed");
	float P = 0.0;
	float D = 0.0;
	
	P = kp*err;

	//if(flag == 0){
	//    flag = 1;
	//}
	//else{
	//    D = kd*(err - prev_err); 
	//}
	//prev_err = err;
    return P+D;
}


