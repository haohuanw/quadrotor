// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz
#define THRESH 1.0

#include "../include/quadcopter_main.h"
#include <time.h>
void read_config(char *);
int processing_loop_initialize();
int path_gen(struct state* localstate);
/*
 * State estimation and guidance thread 
 */
void *processing_loop(void *data){

    int hz = PROC_FREQ;
    processing_loop_initialize();
    int16_t PID_out[8]; //test holding altitude delete after test
    memset(PID_out, 0, 8*sizeof(int16_t));
    float pose[8], set_points[8];
    // Local copies
    struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
    struct state localstate;
    static clock_t state3_start;
    static int safe_buf=5;
    while (1) {  // Main thread loop

        pthread_mutex_lock(&state_mutex);
        memcpy(&localstate, state, sizeof(struct state));
        pthread_mutex_unlock(&state_mutex);

        pthread_mutex_lock(&mcap_mutex);
        mcobs[0] = mcap_obs;
        if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
        else               mcobs[1] = mcap_obs;
        pthread_mutex_unlock(&mcap_mutex);

        //printf("pose z: %lf set_points: %f\n", localstate.pose[2], localstate.set_points[2]);
        localstate.time = mcobs[0]->time;
        localstate.pose[0] = mcobs[0]->pose[0]; // x position
        localstate.pose[1] = mcobs[0]->pose[1]; // y position
        localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
        localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
        localstate.pose[4] = 0;
        localstate.pose[5] = 0;
        localstate.pose[6] = 0;
        localstate.pose[7] = 0;

        // Estimate velocities from first-order differentiation
        double mc_time_step = mcobs[0]->time - mcobs[1]->time;
        if(mc_time_step > 1.0E-7 && mc_time_step < 1){
            localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
            localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
            localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
            localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
        }
        // Geofence activation code
        if(localstate.fence_on == 1){
        //if(1){
            // update desired state
            switch(localstate.state_num){
                case(0):
                    localstate.set_points[0]=mcobs[0]->pose[0];
                    localstate.set_points[1]=mcobs[0]->pose[1];
                    localstate.set_points[2]=mcobs[0]->pose[2];
                    localstate.set_points[3]=mcobs[0]->pose[5];
                    localstate.state_num=1;
                    break;
                case(1):
                    if(localstate.path_ready == 0){
                        int ret = path_gen(&localstate);
                        if(ret == 0){
                            localstate.path_ready = 1;
                            localstate.state_num=2;
                        }
                    }
                    break;
                case(2):
                    update_set_points(&localstate, 1);
                    if(localstate.goal_reached == 1){
                        localstate.state_num=3;
                        state3_start = clock();
                    }
                    break;
                case(3):
                    localstate.current_gripper_cmd = CLOSE;
                    //if(bus.servo[0].cur_load > THRESH && bus.servo[1].cur_load > THRESH){
                    //    localstate.state_num = 4;
                    //}
                    if((clock() - state3_start) > 1*CLOCKS_PER_SEC){
                        localstate.state_num = 4;
                        state3_start = clock();
                    }
                    break;
                case(4):
                    printf("Gripped Done.\n");
//                    if((clock()-state3_start)<1*CLOCKS_PER_SEC){
//                        localstate.motor_off =1;
//                    }
//                    else{
                        //localstate.motor_off=0;
                    
                        while(localstate.unperch != 1){
                            scanf("%d", &localstate.unperch);
                        }
                        if(localstate.unperch == 1){
                            localstate.state_num = 7;
                            state3_start = clock();
                        }
//                    }
                    break;
                case(5):
                    if((clock() - state3_start)<1*CLOCKS_PER_SEC){
                        localstate.motor_off=1;
                    }
                    else{
                        localstate.motor_off = 0;
                        localstate.state_num = 6;
                        state3_start = clock();
                    }
                    break;
                case(6):
                    if((clock() - state3_start)> safe_buf*CLOCKS_PER_SEC){
                    localstate.state_num = 7;
                    state3_start = clock();
                    }
                    break;
                case(7):
                    localstate.current_gripper_cmd = OPEN;
                    if((clock()-state3_start)>1*CLOCKS_PER_SEC){
                    localstate.state_num = 8;
                    }
                    break;
                case(8):
                    localstate.set_points[0]=localstate.goal[0];
                    localstate.set_points[1]=localstate.goal[1];
                    localstate.set_points[2]=localstate.goal[2]-0.5;
                    localstate.set_points[3]=localstate.goal[3];
                    localstate.state_num = 8;
                    break;

            }
            //update_set_points(&localstate,1);
            /*if(localstate.state_num == 1 && localstate.path_ready == 0){
                int ret = path_gen(&localstate);
                if(ret == 0){
                    localstate.path_ready = 1;
                }
            }*/
            //printf("after updating");
            /*for(int i=0;i<8;i++){
              pose[i]=(float)localstate.pose[i];
              set_points[i]=(float)localstate.set_points[i];      
            //printf("%f %f", set_points[i], localstate.set_points[i]);
            }
            auto_control(pose, set_points, PID_out,0);*/
        }

        // Copy to global state (minimize total time state is locked by the mutex)
        pthread_mutex_lock(&state_mutex);
        memcpy(state, &localstate, sizeof(struct state));
        pthread_mutex_unlock(&state_mutex);

        usleep(1000000/hz);

    } // end while processing_loop()

    return 0;
}

/**
 * update_set_points()
 */
int update_set_points(struct state* localstate, int first_time){
    //Add your code here to generate proper reference state for the controller
    float threshold = 0.05;
    float threshold_yaw = 0.1;
    float* set_points = localstate->set_points;
    double* pose = localstate->pose;
    double time = localstate->time;
    double init_time = localstate->time_fence_init;
    printf("setpoints x: %f, y:%f, z: %f\n", set_points[0],set_points[1],set_points[2]);
    printf("pose x: %lf, y: %lf, z: %lf\n", pose[0],pose[1],pose[2]);

    if((fabs(pose[2]-set_points[2])<threshold) 
            && (fabs(pose[0]-set_points[0])<threshold) 
            && (fabs(pose[1]-set_points[1])<threshold) 
            && (fabs(pose[3]-set_points[3])<threshold_yaw) 
      ){	
        //&& ((time - init_time) >= fence_penalty_length)){
        printf("%f", fabs(pose[2]-set_points[2]));
        printf("set_point %d has reached\n", localstate->current_set_point_idx);
        //PID test ONLY
        //localstate->current_set_point_idx = 1;
                //printf("publish next waypoint\n");
        if(localstate->current_set_point_idx != (localstate->num_set_points-1)){
            localstate->current_set_point_idx += 1; //Change to =1 for hovering
            for(int i=0;i<8;++i){
                set_points[i] = localstate->list_of_set_points[localstate->current_set_point_idx][i];
            }
        }
        else{
            localstate->goal_reached = 1;
        }
    }
    return 0;
}


/**
 * processing_loop_initialize()
 */
int processing_loop_initialize(){
    // Initialize state struct
    state = (state_t*) calloc(1,sizeof(*state));
    state->time = ((double)utime_now())/1000000;
    state->current_set_point_idx = 0;
    state->num_set_points = 0;
    state->state_num = 0;
    state->path_ready = 0;
    memset(state->pose,0,sizeof(state->pose));

    // Read configuration file
    char blah[] = "config.txt";

    mcap_obs[0].time = state->time;
    mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

    // Initialize Altitude Velocity Data Structures
    memset(diff_z, 0, sizeof(diff_z));
    memset(diff_z_med, 0, sizeof(diff_z_med));

    // Fence variables
    state->fence_on = 0;
    memset(state->set_points,0,sizeof(state->set_points));
    memset(state->goal,0,sizeof(state->goal));
    state->goal[0] = 0.110322; //bar position. change before flight!!!!!!
    state->goal[1] = -0.119592;
    state->goal[2] = -0.702702;
    state->time_fence_init = 0;
    state->current_gripper_cmd = IDLE;
    state->goal_reached = 0;
    //read_config(blah);
    // Initialize IMU data
    //if(imu_mode == 'u' || imu_mode == 'r'){
    //  imu_initialize();
    //}

    return 0;
}

void read_config(char* config){
    // open configuration file
    FILE* conf = fopen(config,"r");
    // holder string
    //char str[1000];

    // read in the quadrotor initial position
    //fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);
    fscanf(conf, "%lf %lf %lf %lf\n", &(state->pose[0]), &(state->pose[1]), &(state->pose[2]), &(state->pose[3]));
    fscanf(conf, "%f %f %f %f\n", &(state->goal[0]), &(state->goal[1]), &(state->goal[2]), &(state->goal[3]));
    printf("%lf %lf %lf %lf\n", (state->pose[0]), (state->pose[1]), (state->pose[2]), (state->pose[3]));
    printf("%lf %lf %lf %lf\n", (state->goal[0]), (state->goal[1]), (state->goal[2]), (state->goal[3]));
    while(fscanf(conf, "%f %f %f %f\n", 
                &(state->list_of_set_points[state->num_set_points][0]), 
                &(state->list_of_set_points[state->num_set_points][1]),
                &(state->list_of_set_points[state->num_set_points][2]),
                &(state->list_of_set_points[state->num_set_points][3]) ) != EOF){
    
        printf("%lf\n", state->list_of_set_points[state->num_set_points][2]);
        ++state->num_set_points;
    } 
    // ADD YOUR CODE HERE
    for(int i=0; i<3; ++i){
        state->set_points[i] = state->list_of_set_points[0][i];
    }	
    fclose(conf);
}

//only handle descending perching
int path_gen(struct state* localstate){
    localstate->current_set_point_idx = 0;
    localstate->num_set_points = 0;
    float curr_setpoint[4];
    curr_setpoint[0] = (float) localstate->goal[0];
    curr_setpoint[1] = (float) localstate->goal[1];
    curr_setpoint[2] = (float) localstate->pose[2];
    curr_setpoint[3] = (float) localstate->goal[3];
    while(fabs(localstate->goal[2] - curr_setpoint[2]) > 0.3){
        localstate->list_of_set_points[localstate->num_set_points][0] = curr_setpoint[0];
        localstate->list_of_set_points[localstate->num_set_points][1] = curr_setpoint[1];
        localstate->list_of_set_points[localstate->num_set_points][2] = curr_setpoint[2];
        localstate->list_of_set_points[localstate->num_set_points][3] = curr_setpoint[3];
        ++localstate->num_set_points;
        curr_setpoint[2] += 0.3;
        if(localstate->num_set_points == 9){
            return 1;
        }
    }
    localstate->list_of_set_points[localstate->num_set_points][0] = localstate->goal[0];
    localstate->list_of_set_points[localstate->num_set_points][1] = localstate->goal[1];
    localstate->list_of_set_points[localstate->num_set_points][2] = localstate->goal[2];
    localstate->list_of_set_points[localstate->num_set_points][3] = localstate->goal[3];
    ++localstate->num_set_points;
    printf("start: %f %f %f\n", localstate->pose[0], localstate->pose[1], localstate->pose[2]);
    for(int i=0; i<localstate->num_set_points; ++i){
        printf("waypoint: %f %f %f\n", localstate->list_of_set_points[i][0], localstate->list_of_set_points[i][1], localstate->list_of_set_points[i][2]);
    }
    return 0;
}
