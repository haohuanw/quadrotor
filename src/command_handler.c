// Handler function to either pass through RX commands to Naza or else
// copy computer (autonomous control) commands through to Naza.
#define EXTERN extern

#include "../include/quadcopter_main.h"

////////////////////////////////////////////////////////////////////////

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
        const channels_t *msg, void *userdata)
{
    // create a copy of the received message
    // newmsg contains trpy and autoswitch
    channels_t new_msg;
    new_msg.utime = msg->utime;
    new_msg.num_channels = msg->num_channels;
    new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
    for(int i = 0; i < msg->num_channels; i++){
        new_msg.channels[i] = msg->channels[i];
        //printf("%d\n", msg->channels[i]);
    }
    // Copy state to local state struct to minimize mutex lock time
    struct state localstate;
    pthread_mutex_lock(&state_mutex);
    memcpy(&localstate, state, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    //Change fence_on variable to channel 8 output
    localstate.fence_on = new_msg.channels[msg->num_channels-1] > 1700;

    // Decide whether or not to edit the motor message prior to sending it
    // set_points[] array is specific to geofencing.  You need to add code 
    // to compute them for our FlightLab application!!!
    struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
    pthread_mutex_lock(&mcap_mutex);  
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);

    float pose[8], set_points[8];
    if(localstate.fence_on == 1){
        printf("FENCE ON");
        for(int i = 0; i < 8; i++){
            pose[i] = (float) localstate.pose[i];
            set_points[i] = localstate.set_points[i];
        }
        /*switch(localstate.state_num){
                        case(4):
                //turn off quadrotor need flying data
                if((mcobs[1]->time - localstate.init_command_time)< 1000000){
                    new_msg.channels[0]=1000;//thrust 
                    new_msg.channels[1]=1000;//roll
                    new_msg.channels[2]=1000;//pitch
                    new_msg.channels[3]=1000;//yaw
                    }
                else{	
                    scanf("%d", (&localstate)->unperch);
                    if(localstate.unperch == 1){
                        localstate.state_num=5;
                        localstate.init_command_time=mcobs[1]->time;
                        }
                    }
                break;		
            case(5):
                //turn on motor and give take off thrust
                if((mcobs[1]->time - localstate.init_command_time)< 1000000){
                    new_msg.channels[0]=1000;//thrust 
                    new_msg.channels[1]=1000;//roll
                    new_msg.channels[2]=1000;//pitch
                    new_msg.channels[3]=1000;//yaw
                    }
                else {	
                    new_msg.channels[0]=1600;//needs more thought on this
                    localstate.state_num=6;
                }
                break;
            case(6):
                //gripper release
                localstate.current_gripper_cmd = OPEN;
                localstate.state_num=7;
                break;
            case(7):
                //fly up and hold
                set_points[0]= 1; //hard code a position 
                set_points[1]= 1; //based on bar position
                set_points[2]= 1;
                set_points[3]= 1;
                localstate.state_num=7;
            case(9):
                //disturbance handler state
                set_points[0]= 1; //set a evac position 
                set_points[1]= 1; //based on bar position
                set_points[2]= 1; //here or processing loop?
                set_points[3]= 1;
                localstate.current_set_point_idx=0;
                localstate.state_num = 2;
        }*/
        // hold position at edge of fence
        // This needs to change - mutex held way too long
       /*
        *localstate.goal[0] = 27/1000;
        *localstate.goal[1]=29/1000;
        *localstate.goal[2] = 900/1000;
        *localstate.goal[3] = 0;
        *localstate.goal[4] = 0;
        *localstate.goal[5] = 0;
        *localstate.goal[6] = 0;
        *localstate.goal[7] = 0;
        */

        /*if(localstate.current_gripper_cmd != CLOSE){
            localstate.current_gripper_cmd = CLOSE;
            
        }*/
        if(((mcobs[0]->time - mcobs[1]->time)< 1.5)){
            auto_control(pose, set_points, new_msg.channels,0,localstate.motor_off);
        }
        else if((localstate.state_num == 0) || (localstate.state_num == 6)){
            auto_control(pose, set_points, new_msg.channels,1,0);
        }
        else{
            auto_control(pose, set_points, new_msg.channels,1,0);
        }
        //printf("FENCE ON\n");
    } else{  // Fence off
        // pass user commands through without modifying
        if(localstate.current_gripper_cmd != OPEN){
            localstate.current_gripper_cmd = OPEN;
        }
		localstate.state_num=0;
		localstate.path_ready=0;
		localstate.unperch=0;
        printf("FENCE OFF\n");
        //clear flags set in fence-on
    }
    // send lcm message to motors
    channels_t_publish((lcm_t *) userdata, "CHANNELS_1_TX", &new_msg);
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);
    
    // Save received (msg) and modified (new_msg) command data to file.
    // NOTE:  Customize as needed (set_points[] is for geofencing)
    fprintf(block_txt,"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f\n",
            (long int) msg->utime,msg->channels[0],msg->channels[1],msg->channels[2],
            msg->channels[3], msg->channels[7],
            new_msg.channels[0],new_msg.channels[1],new_msg.channels[2], 
            new_msg.channels[3],new_msg.channels[7],
            set_points[0],set_points[1],set_points[2],
            set_points[3],set_points[4],set_points[5],set_points[6],
            set_points[7]);
    fflush(block_txt);
}
